# The Cascade

Slides about the CSS cascade.

- Author: [Fabian](https://www.fabian-kurz.de/)
- Build with [slidev](https://sli.dev/)

## Content


## Links

Useful ressources

- [CSS Cascading and Inheritance Level 6](https://drafts.csswg.org/css-cascade-6/)
- [W3C: Specificity rules](https://www.w3.org/TR/selectors-4/#specificity-rules)
- [The CSS Cascade, a deep dive | Bramus Van Damme | CSS Day 2022](https://yewtu.be/watch?v=zEPXyqj7pEA)
- [Cascade as funnel | Miriam Suzanne](https://slides.oddbird.net/demos/cascade/funnel/)
- [CSS Cascade Layers Demo | Stacy Kvernmo](https://codepen.io/stacy/pen/ExGVEMV?editors=1100)
- [Cascade Work Template | Miriam Suzanne](https://codepen.io/miriamsuzanne/pen/abPmNLB)
- [CSS @scope | 12daysofweb](https://12daysofweb.dev/2023/css-scope/)