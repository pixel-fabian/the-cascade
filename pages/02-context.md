---
layout: center
---

# (Shadow) Context

---

<h2 class="mb-4">normal</h2>

1. Light DOM
2. Shadow DOM

<h2 class="mb-4 mt-16">!important</h2>

1. Shadow DOM
2. Light DOM

---

<iframe height="300" style="width: 100%;" scrolling="no" title="Style Shadow DOM" src="https://codepen.io/pixel-fabian/embed/preview/oNObrdx?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/pixel-fabian/pen/oNObrdx">
  Style Shadow DOM</a> by Fabian (<a href="https://codepen.io/pixel-fabian">@pixel-fabian</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

<!--
- Example: custom-element 
- Style in "template" block sits in shadow DOM
- Color "red" is overwritten by CSS in light DOM
- Set color in shadow DOM to !important => Override
- Set color in light DOM to !important => No effect
-->