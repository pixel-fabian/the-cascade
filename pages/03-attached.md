---
layout: center
---

# Element Attached Styles

---

<h2 class="mb-4">normal</h2>

1. Inline Styles
2. Selectors

<h2 class="mb-4 mt-16">!important</h2>

1. Inline Styles
2. Selectors

---

<iframe height="300" style="width: 100%;" scrolling="no" title="Cascade 03 - Element-Attached Style" src="https://codepen.io/pixel-fabian/embed/dyLbWZV?default-tab=html%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/pixel-fabian/pen/dyLbWZV">
  Cascade 03 - Element-Attached Style</a> by Fabian (<a href="https://codepen.io/pixel-fabian">@pixel-fabian</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

<!--
- Inline-Style: rebeccapurple wins
- Selector: !important => override
- Inline-Style: !important => override
- Different to shadow context!
- Class or ID selector does not matter. Specificity comes later in cascade.
-->