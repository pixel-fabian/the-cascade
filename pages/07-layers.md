---
layout: center
---

# Layers

---
 
```css
// reset
ul[class] {
  margin: 0;
  padding: 0;
  list-style-type: none;
}

.nav {
  margin: 0 40px;
}
```

---

```css
@layer {
  ul[class] {
    margin: 0;
    padding: 0;
    list-style-type: none;
  }
}

@layer {
  .nav {
    margin: 0 40px;
  }
}
```

---

```css
@layer reset {
  ul[class] {
    margin: 0;
    padding: 0;
    list-style-type: none;
  }
  ...
}

@layer base {
  ...
}

@layer components {
  .nav {
    margin: 0 40px;
  }
}

@layer utilities {
  ...
}
```

---


<h2 class="mb-4">normal</h2>

1. un-layered
2. last layer
3. ...
4. first layer

---

```css
@layer reset, base, components, utilities;

@layer components {
  .nav {
    margin: 0 40px;
  }
}

@layer reset {
  ul[class] {
    margin: 0;
    padding: 0;
    list-style-type: none;
  }
  ...
}

@layer components {
  button {
    border-radius: 10px;
  }
}

```

---

<h2 class="mb-4">!important</h2>

1. first-layer
2. ...
3. last layer
4. un-layered