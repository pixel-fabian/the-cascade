---
layout: center
---

# Order of Appearance

---

The last declaration in document order wins

<iframe height="300" style="width: 100%;" scrolling="no" title="Cascade 01 - conflict" src="https://codepen.io/pixel-fabian/embed/preview/zYbJeGv?default-tab=css&editable=true&theme-id=dark" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/pixel-fabian/pen/zYbJeGv">
  Cascade 01 - conflict</a> by Fabian (<a href="https://codepen.io/pixel-fabian">@pixel-fabian</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>