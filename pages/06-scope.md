---
layout: center
---

# Scope Proximity

---
layout: two-cols
---

```html
<article class="feature">
  <section class="article-hero">
    <h2>Title</h2>
    <img src="hero-image.jpg">
  </section>

    <section class="article-body">
    <h3>Subtitle</h2>
    <p>Teaser Text</p>
    <img src="teaser-image.jpg">
    <p>More Text</p>
    <figure>
      <img src="anotherimage.jpg">
      <figcaption>Caption of this image</figcaption>
    </figure>
  </section>

  <footer>
    <p>Text in the footer</p>
    <img src="footerimage.jpg">
  </footer>
<article>
```

::right::

```css
.article-body img {
  border: 5px solid black;
  background-color: goldenrod;
}
```

<v-click>
```css
@scope (.article-body) {
  img {
    border: 5px solid black;
    background-color: goldenrod;
  }
}

```
</v-click>

<!--
- Want to target all images directly within the article body
-->

---
layout: two-cols
---

```html
<article class="feature">
  <section class="article-hero">
    <h2>Title</h2>
    <img src="hero-image.jpg">
  </section>

    <section class="article-body">
    <h3>Subtitle</h2>
    <p>Teaser Text</p>
    <img src="teaser-image.jpg">
    <p>More Text</p>
    <figure>
      <img src="anotherimage.jpg">
      <figcaption>Caption of this image</figcaption>
    </figure>
  </section>

  <footer>
    <p>Text in the footer</p>
    <img src="footerimage.jpg">
  </footer>
<article>
```

[@scope (mdn)](https://developer.mozilla.org/en-US/docs/Web/CSS/@scope), [@scope (caniuse)](https://caniuse.com/css-cascade-scope)


::right::

```css
.feature > .article-body > img {
  border: 5px solid black;
  background-color: goldenrod;
}
```

<v-click>
```css
@scope (.article-body) to (figure) {
  img {
    border: 5px solid black;
    background-color: goldenrod;
  }
}

```
</v-click>

<!--
- Want to target only the image directly within the article body
- Before: Tightly coupled to HTML structure
- Could use class names
- Simple: New @scope rule
- Not supported by firefox (yet)
-->

---

### Scope in Vue.js

![Scope Vue](/scope_vue.jpg)

<!--
- Style that is scoped to a component
- We don't want other styles in our app to be affected
- As developer, only add 'scoped'
- Vue: Add scope attribute to every element of that component
- Vue: Add attribute selectors to every rule via PostCSS
- Quite complicated
-->

---

### Scope in Vue.js

<img src="/scope_vue-2.jpg" class="h-100" />

[State of Vuenion 2023](https://piped.adminforge.de/watch?v=y-hN5Q_lb9A)

<!--
- In HTML:
- Only need a scope attribute at the root of a component
- Not at each element within the component
-->



---

### Scope in Vue.js

<img src="/scope_vue-3.jpg" class="h-100" />

[State of Vuenion 2023](https://piped.adminforge.de/watch?v=y-hN5Q_lb9A)

<!--
- In CSS:
- Need to wrap the CSS for the component in a @scope block
- No need to transform every rule of the component
- Simpler and more performant
-->