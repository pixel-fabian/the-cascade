---
layout: center
---

# Specificity

---
layout: center
---

<div class="text-4xl mb-16">The declaration with the <strong>highest specificity</strong> wins.</div>

[W3C: Specificity rules](https://www.w3.org/TR/selectors-4/#specificity-rules)

---
layout: center
---

<div class="text-6xl">(A, B, C)</div>

<!--
- Specificity is calculated in 3 different numbers
-->

---
layout: center
---

<div class="text-6xl">
(<span style="color:orange">0</span>, <span style="color:aqua">0</span>, <span style="color:fuchsia">0</span>)
</div>

---
layout: center
---

<div class="text-6xl mb-8 text-center">
(<span style="color:orange">0</span>, <span style="color:aqua">0</span>, <span style="color:fuchsia">1</span>)
</div>

<span class="text-4xl" style="color:fuchsia">`span`</span>

<!-- 
- Element selectors have the lowest specificity
- e.g. header, main, span, a, h1, h2, h3
-->

---
layout: center
---

<div class="text-6xl mb-8 text-center">
(<span style="color:orange">0</span>, <span style="color:aqua">1</span>, <span style="color:fuchsia">0</span>)
</div>

<span class="text-4xl" style="color:aqua">`.my-class`, `:hover`, `[type="input"]`</span>

<!-- 
- class, pseudo-class, attribute selector
-->

---
layout: center
---

<div class="text-6xl mb-8 text-center">
(<span style="color:orange">1</span>, <span style="color:aqua">0</span>, <span style="color:fuchsia">0</span>)
</div>

<span class="text-4xl" style="color:orange">`#my-id`</span>

<!-- 
- id selector
-->

---
layout: center
---

<div class="text-6xl mb-8 text-center">
(<span style="color:orange">1</span>, <span style="color:aqua">0</span>, <span style="color:fuchsia">3</span>)
</div>

<span class="text-4xl" style="color:orange">`#my-id`</span><span class="text-4xl" style="color:fuchsia">`ul li a`</span>

<!-- 
- In each group gets added
-->

---
layout: center
---

<div class="text-6xl mb-8 text-center">
(<span style="color:orange">A</span>, <span style="color:aqua">B</span>, <span style="color:fuchsia">C</span>)
</div>


<div class="text-4xl">
<span style="color:fuchsia"><code>header</code></span>
<span style="color:orange"><code>#mainnav</code></span>
<span style="color:fuchsia"><code>ul</code></span>
<span style="color:aqua"><code>.secondary</code></span>
<span style="color:fuchsia"><code>li</code></span>
<code>></code>
<span style="color:fuchsia"><code>a</code></span>
</div>

<!-- 
- What is the specificity here?
-->

---
layout: center
---

<div class="text-6xl mb-8 text-center">
(<span style="color:orange">1</span>, <span style="color:aqua">1</span>, <span style="color:fuchsia">4</span>)
</div>

<div class="text-4xl">
<span style="color:fuchsia"><code>header</code></span>
<span style="color:orange"><code>#mainnav</code></span>
<span style="color:fuchsia"><code>ul</code></span>
<span style="color:aqua"><code>.secondary</code></span>
<span style="color:fuchsia"><code>li</code></span>
<code>></code>
<span style="color:fuchsia"><code>a</code></span>
</div>

<!-- 
- 1x id
- 1x class
- 4x element
-->

---
layout: center
---

<div class="text-6xl mb-8 text-center">
(<span style="color:orange">1</span>, <span style="color:aqua">0</span>, <span style="color:fuchsia">0</span>)
</div>

<span class="text-4xl" style="color:orange">`#my-id`</span>

<div class="text-6xl mb-8 mt-10 text-center">
(<span style="color:orange">0</span>, <span style="color:aqua">1</span>, <span style="color:fuchsia">3</span>)
</div>

<span class="text-4xl" style="color:aqua">`.secondary`</span>
<span class="text-4xl" style="color:fuchsia">`ul li a`</span>

<!-- 
- Competing declarations. Which one wins?
- Comparison from left to right
- Top wins: 1:0
- Second & third part are ignored
-->

---

```html
<div class="login">
  <label for="email">E-Mail</label>
  <input type=email id="email" class="inputfield" value="test@supermail.com" />
</div>
```

```css
body {
  color: #333;                              /* A,B,C */
}

.login .inputfield {
  color: blueviolet;                        /* A,B,C */
}

#email {
  color: darkgoldenrod;                     /* A,B,C */
}

input[type="email"] {
  color: magenta;                           /* A,B,C */
}

.login label + .inputfield {
  color: blue;                              /* A,B,C */
}
```

---

```html
<div class="login">
  <label for="email">E-Mail</label>
  <input type=email id="email" class="inputfield" value="test@supermail.com" />
</div>
```

```css
body {
  color: #333;                              /* 0,0,1 */
}

.login .inputfield {
  color: blueviolet;                        /* A,B,C */
}

#email {
  color: darkgoldenrod;                     /* A,B,C */
}

input[type="email"] {
  color: magenta;                           /* A,B,C */
}

.login label + .inputfield {
  color: blue;                              /* A,B,C */
}
```

---

```html
<div class="login">
  <label for="email">E-Mail</label>
  <input type=email id="email" class="inputfield" value="test@supermail.com" />
</div>
```

```css
body {
  color: #333;                              /* 0,0,1 */
}

.login .inputfield {
  color: blueviolet;                        /* 0,2,0 */
}

#email {
  color: darkgoldenrod;                     /* A,B,C */
}

input[type="email"] {
  color: magenta;                           /* A,B,C */
}

.login label + .inputfield {
  color: blue;                              /* A,B,C */
}
```

---

```html
<div class="login">
  <label for="email">E-Mail</label>
  <input type=email id="email" class="inputfield" value="test@supermail.com" />
</div>
```

```css
body {
  color: #333;                              /* 0,0,1 */
}

.login .inputfield {
  color: blueviolet;                        /* 0,2,0 */
}

#email {
  color: darkgoldenrod;                     /* 1,0,0 */
}

input[type="email"] {
  color: magenta;                           /* A,B,C */
}

.login label + .inputfield {
  color: blue;                              /* A,B,C */
}
```

---

```html
<div class="login">
  <label for="email">E-Mail</label>
  <input type=email id="email" class="inputfield" value="test@supermail.com"  />
</div>
```

```css
body {
  color: #333;                              /* 0,0,1 */
}

.login .inputfield {
  color: blueviolet;                        /* 0,2,0 */
}

#email {
  color: darkgoldenrod;                     /* 1,0,0 */
}

input[type="email"] {
  color: magenta;                           /* 0,1,1 */
}

.login label + .inputfield {
  color: blue;                              /* A,B,C */
}
```

---

```html
<div class="login">
  <label for="email">E-Mail</label>
  <input type=email id="email" class="inputfield" value="test@supermail.com" />
</div>
```

```css
body {
  color: #333;                              /* 0,0,1 */
}

.login .inputfield {
  color: blueviolet;                        /* 0,2,0 */
}

#email {
  color: darkgoldenrod;                     /* 1,0,0 WINNER */
}

input[type="email"] {
  color: magenta;                           /* 0,2,0 */
}

.login label + .inputfield {
  color: blue;                              /* 0,2,1 */
}
```
---
layout: center
---

<div class="text-6xl mb-8 text-center">
(<span style="color:orange">A</span>, <span style="color:aqua">B</span>, <span style="color:fuchsia">C</span>)
</div>

<span class="text-4xl">`ul > li:is(#highlighted, .active)`</span>

<!--
- Of course there are exceptions
-->

---
layout: center
---

<div class="text-6xl mb-8 text-center">
(<span style="color:orange">A</span>, <span style="color:aqua">B</span>, <span style="color:fuchsia">C</span>)
</div>

<span class="text-4xl">`ul > li:is(#highlighted, .active)`</span>

<span class="text-4xl">
<pre>
ul > li#highlighted,
ul > li.active
</pre>
</span>

<!--
- Selects any element by one of the selectors in that list
- Can be rewritten
-->

---
layout: center
---

<div class="text-6xl mb-8 text-center">
(<span style="color:orange">1</span>, <span style="color:aqua">0</span>, <span style="color:fuchsia">2</span>)
</div>

<div class="text-4xl mb-24">
<span style="color:fuchsia"><code>ul</code></span>
<code>></code>
<span style="color:fuchsia"><code>li</code></span>
<code>:is(</code>
<span style="color:orange"><code>#highlighted</code></span>
<code>, .active)</code>
</div>

[:is()](https://developer.mozilla.org/en-US/docs/Web/CSS/:is), [:not()](https://developer.mozilla.org/en-US/docs/Web/CSS/:not), [:has()](https://developer.mozilla.org/en-US/docs/Web/CSS/:has)

---
layout: center
---

<div class="text-6xl mb-8 text-center">
(<span style="color:orange">A</span>, <span style="color:aqua">B</span>, <span style="color:fuchsia">C</span>)
</div>

<span class="text-4xl">`ul > li:where(#highlighted, .active)`</span>

<span class="text-4xl">
<pre>
ul > li#highlighted,
ul > li.active
</pre>
</span>

---
layout: center
---

<div class="text-6xl mb-8 text-center">
(<span style="color:orange">0</span>, <span style="color:aqua">0</span>, <span style="color:fuchsia">2</span>)
</div>

<div class="text-4xl mb-24">
<span style="color:fuchsia"><code>ul</code></span>
<code>></code>
<span style="color:fuchsia"><code>li</code></span>
<code>:where(#highlighted, .active)</code>
</div>

[:where()](https://developer.mozilla.org/en-US/docs/Web/CSS/:where)

<!--
- Unlike :is(), neither the :where pseudo-class, nor any of its arguments contribute to the specificity of the selector—its specificity is always zero.
-->

---
layout: center
---

<div class="text-6xl mb-8 text-center">
(<span style="color:orange">A</span>, <span style="color:aqua">B</span>, <span style="color:fuchsia">C</span>)
</div>

<span class="text-4xl">`li:nth-child(2n + 1 of div, .highlight)`</span>

---
layout: center
---

<div class="text-6xl mb-8 text-center">
(<span style="color:orange">0</span>, <span style="color:aqua">2</span>, <span style="color:fuchsia">1</span>)
</div>

<div class="text-4xl">
<span style="color:fuchsia"><code>li</code></span>
<span style="color:aqua"><code>:nth-child</code></span>
<code>(2n + 1 of div,</code>
<span style="color:aqua"><code>.highlight</code></span>
<code>)</code>
</div>

<!--
- The specificity of an :nth-child() or :nth-last-child() selector is the specificity of the pseudo-class itself plus the specificity of the most specific complex selector in its selector list argument
-->

---
layout: center
---

<img src="/specificity_calculator.jpg" class="h-100" />

[https://polypane.app/css-specificity-calculator](https://polypane.app/css-specificity-calculator)

---

![css-specificity-devtools](/specificity_devtools.jpg)