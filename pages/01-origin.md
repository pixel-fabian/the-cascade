---
layout: center
---

# Origin and importance

---

<h2 class="mb-4">Origins</h2>

1. 
2.
3.
4.
5. 
6. <div v-click="2"> Author (Developer) </div>
7. <div v-click="1"> User (Settings) </div>
8. User Agent (Browser) 

---
layout:center
---

### User Agent Styles

<img src="/origin_user-agent.jpg" class="h-100" />

[Chrome](https://chromium.googlesource.com/chromium/blink/+/master/Source/core/css/html.css), [Firefox](https://searchfox.org/mozilla-central/source/layout/style/res/html.css)

<!--
- Default provided by the browser
- Might differ form browser to browser
- Override by author: list-style-type
-->

---

### User Settings

![User Settings](/origin_user-settings.jpg)

<!--
- Settings in the browser
- Example font-size: Change and see how website react to it
- If you (author) are using fixed sizes, override user settings. => Use relative units like "em" or "rem"
-->

---

<h2 class="mb-4">Origins</h2>

1. 
2. <div v-click="2">!important User Agent (browser)</div>
3. <div v-click="1">!important User (settings)</div>
4. !important Author (developer)
5. 
6. Author (developer)
7. User (settings)
8. User Agent (browser)

<!--
- Settings in the browser
- Example font-size: Change and see how website react to it
- If you (author) are using fixed sizes, override user settings. => Use relative units like "em" or "rem"
-->

---

### User Settings !important

![User Settings](/origin_user-important.jpg)

<!--
- Browser settings can override author
- Example: sans-serif font is "28 days later"
-->

---

### User Agent !important

![User Agent Important](/origin_user-agent-important.jpg)

<!--
- Few user agent styles are marked with important
- Can't change them as user or author
- Examples: left Chrome, right FF
- Chrome: input[type="file"] text-align marked as !important
-->

---

<h2 class="mb-4">Origins</h2>

1. Transition
2. !important User Agent (browser)
3. !important User (user)
4. !important Author (developer)
5. Animation
6. Author (developer)
7. User (settings)
8. User Agent (browser)

<!--
- 5. CSS Animations
- 1. CSS Transitions
-->