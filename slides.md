---
theme: seriph
background: /waterfall.jpg
class: text-center
highlighter: shiki
lineNumbers: false
drawings:
  persist: false
transition: slide-left
title: The Cascade
---

# The Cascade

---
layout: center
---

<img src="/meme-anakin.webp" class="h-120" />


---
layout: center
---
<div class="text-4xl mb-16"><strong>Cascading</strong> Style Sheets</div>

<blockquote class="mb-4">
The proposed scheme supplies the brower with an ordered list (cascade) of style sheets. 
</blockquote>

> The style sheet scheme is designed so that style sheets can be cascaded; the user/browser specifies initial preferences and hands the remaining influence over to the style sheets referenced in the incoming document.

[Cascading HTML style sheets -- a proposal](https://www.w3.org/People/howcome/p/cascade.html) (Håkon W Lie, 1994)


<!-- 
-  From beginning (frist proposal) "cascading" was included
-->

---

## Conflicts

<iframe height="300" style="width: 100%;" scrolling="no" title="Cascade 01 - conflict" src="https://codepen.io/pixel-fabian/embed/preview/zYbJeGv?default-tab=css&editable=true&theme-id=dark" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/pixel-fabian/pen/zYbJeGv">
  Cascade 01 - conflict</a> by Fabian (<a href="https://codepen.io/pixel-fabian">@pixel-fabian</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

<!-- 
- Simple button with two styles
- Which color hast the text of the button?
- Solution: Red
-->

---

![Thunderdome](/meme-thunderdome.webp)

<!--
- Thunderdome: Only one can survive
- For each element on the page, the styles must be unambiguous
-->
---

## More Conflicts

<iframe height="300" style="width: 100%;" scrolling="no" title="Cascade 02 - more conflicts" src="https://codepen.io/pixel-fabian/embed/XWGPwyp?default-tab=html&editable=true&theme-id=dark" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/pixel-fabian/pen/XWGPwyp">
  Cascade 02 - more conflicts</a> by Fabian (<a href="https://codepen.io/pixel-fabian">@pixel-fabian</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

<!-- 
- E-Mail input with label
- Lots of conflicting declerations
- Attached style in HTML as well
- Which color hast the text of the input?
- Solution: blueviolet
- Remove important: 2. cyan, 3. green
-->

---
layout: center
---

<span class="text-4xl mb-16">
The CSS Cascade is the <strong>algorithm</strong> that <strong>determins the winner</strong> from a group of <strong>competing declerations</strong>
</span>

[@bramus](https://yewtu.be/watch?v=zEPXyqj7pEA)

<!-- 
- Nice defintion by Bramus Van Damme
- Recommend his talk: The CSS Cascade, a deep dive | CSS Day 2022
-->

---
layout: two-cols
---

<h2 class="mb-4">The Cascade </h2>

1. Origin and importance
2. (Shadow) Context
3. Element Attached Styles
4. Layers
5. Specificity
6. Scope Proximity
7. Order of Appearance

[CSS Cascading and Inheritance Level 6](https://drafts.csswg.org/css-cascade-6/)

::right::

<div v-click="1">
<img src="/cascade_coin-sorter-diy.jpg" />
</div>

<!-- 
- Current state of CSS cascading (December 2023)
- Scope and layers are quite new
- Works like a coin sorter: First rule that solves a conflict is applied. All further rules are ignored
- Example: If two declarations from different origins are in conflict, only that matters
-->

---
src: ./pages/01-origin.md
---

---
src: ./pages/02-context.md
---

---
src: ./pages/03-attached.md
---

---
layout: center
---

# ~~Layers~~

<!--
- Next in the cascade are layers
- Come back to this later
-->

---
src: ./pages/04-specificity.md
---

---
src: ./pages/05-order.md
---

---

<h2 class="mb-4">The Cascade </h2>

1. Origin and importance ✅
2. (Shadow) Context ✅
3. Element Attached Styles ✅
4. Layers
5. Specificity ✅ <v-click>💥</v-click>
6. Scope Proximity
7. Order of Appearance ✅


---
src: ./pages/06-scope.md
---

---
src: ./pages/07-layers.md
---



---
layout: center
---

<img src="/meme-einstein.webp" class="h-120" />


---

# Links

- [CSS Cascading and Inheritance Level 6](https://drafts.csswg.org/css-cascade-6/)
- [W3C: Specificity rules](https://www.w3.org/TR/selectors-4/#specificity-rules)
- [The CSS Cascade, a deep dive | Bramus Van Damme | CSS Day 2022](https://yewtu.be/watch?v=zEPXyqj7pEA)
- [Cascade as funnel | Miriam Suzanne](https://slides.oddbird.net/demos/cascade/funnel/)
- [CSS Cascade Layers Demo | Stacy Kvernmo](https://codepen.io/stacy/pen/ExGVEMV?editors=1100)
- [Cascade Work Template | Miriam Suzanne](https://codepen.io/miriamsuzanne/pen/abPmNLB)
- [CSS @scope | 12daysofweb](https://12daysofweb.dev/2023/css-scope/)
